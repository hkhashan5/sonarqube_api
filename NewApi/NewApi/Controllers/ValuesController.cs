﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace NewApi.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
      /*  public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }*/

        // GET api/values/5
        public HttpResponseMessage Get(String id)
        {
            if (id == "values")
            {
                var stream = new FileStream(@"C:\Users\Delegate\Desktop\repos\NewApi\NewApi\JSON\Sonarvalue.json", FileMode.Open);
                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return result;
            }
            else
            {
                return null;
            }
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
